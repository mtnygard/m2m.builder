package main

import (
	"bytes"
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"text/template"

	"github.com/google/go-github/github"
)

// GistID tells where the files come from
var GistID = "9ea6477fcd77596566fb9f179eea4d44"

// A Student is one human individual in one class
type Student struct {
	ID        string `csv:"id"`
	Name      string `csv:"name"`
	Email     string `csv:"email"`
	PublicKey string `csv:"publicKey"`
	Username  string `csv:"username"`
	Password  string `csv:"password"`
	Region    string
	Zones     []string
}

func main() {
	if len(os.Args) > 1 {
		s, err := readStudent(os.Args[1])
		if err != nil {
			panic(err)
		}
		err = checkStudent(s)
		if err != nil {
			panic(err)
		}
		err = emitTemplates(s)
		if err != nil {
			panic(err)
		}
	} else {
		err := emitStarter(os.Stdout)

		if err != nil {
			panic(err)
		}
	}
}

func emitStarter(w io.Writer) error {
	var student Student
	b, err := json.Marshal(&student)
	if err != nil {
		return err
	}
	var out bytes.Buffer
	json.Indent(&out, b, "", "\t")
	_, err = out.WriteTo(w)
	return err
}

func readStudent(file string) (Student, error) {
	var student Student
	configFile, err := os.Open(file)
	defer configFile.Close()
	if err != nil {
		return student, err
	}
	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&student)
	return student, nil
}

func checkStudent(student Student) error {
	if len(student.Username) > 16 {
		return errors.New("MySQL requires the username to be 16 characters or less")
	}

	matched, _ := regexp.MatchString("-", student.Username)

	if matched {
		return errors.New("Student names cannot have '-'. Modify " + student.Username)
	}

	if student.ID == "" {
		return errors.New("id must be filled in")
	}

	if student.Username == "" {
		return errors.New("username must be filled in")
	}

	if student.Password == "" {
		return errors.New("password must be filled in")
	}

	if student.PublicKey == "" {
		return errors.New("publicKey must be filled in")
	}

	matched, _ = regexp.MatchString("BEGIN SSH2 PUBLIC KEY", student.PublicKey)

	if matched {
		return errors.New("publicKey looks like a Putty key. Run 'ssh-keygen -i -f <keyfile>.pub' and use the output instead")
	}

	if !strings.HasPrefix(student.PublicKey, "ssh") {
		return errors.New("publicKey does not look like OpenSSH format")
	}

	return nil
}

func emitTemplates(student Student) error {
	funcMap := template.FuncMap{
		"sep": sep,
	}
	client := github.NewClient(nil)
	ctx := context.Background()

	gist, _, err := client.Gists.Get(ctx, GistID)

	if err != nil {
		return err
	}

	for _, f := range gist.Files {
		fmt.Printf("Processing file %s\n", *f.Filename)
		nametmpl, err := template.New(f.GetRawURL()).Funcs(funcMap).Parse(*f.Filename)

		if err != nil {
			return err
		}

		var fname strings.Builder
		err = nametmpl.Execute(&fname, student)

		if err != nil {
			return err
		}

		fmt.Printf("Creating file %s.\n", fname.String())

		tmpl, err := template.New(*f.Filename).Parse(*f.Content)

		if err != nil {
			return err
		}

		p := fname.String()

		if strings.Contains(p, pathsep) {
			err = os.MkdirAll(filepath.Dir(p), os.ModePerm)
			if err != nil {
				return err
			}
		}

		outfile, err := os.Create(p)
		if err != nil {
			return err
		}
		defer outfile.Close()
		err = tmpl.Execute(outfile, student)
		if err != nil {
			return err
		}
	}
	return nil
}

var pathsep = fmt.Sprintf("%c", os.PathSeparator)

func sep() string {
	return pathsep
}
